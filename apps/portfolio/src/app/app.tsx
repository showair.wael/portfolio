import React, { useEffect, useState } from 'react'
import { Message } from '@portfolio.workspace/api-interfaces'

export const App = () => {
  const [m, setMessage] = useState<Message>({ message: '' })

  useEffect(() => {
    fetch('/api')
      .then((r) => r.json())
      .then(setMessage)
  }, [])

  return (
    <>
      <div style={{ textAlign: 'center' }}>
        <h1>Welcome to portfolio!</h1>
        <img
          width='450'
          src='https://raw.githubusercontent.com/nrwl/nx/master/images/nx-logo.png'
          alt='nx logo'
        />
      </div>
      <div>{m.message}</div>
    </>
  )
}

export default App
